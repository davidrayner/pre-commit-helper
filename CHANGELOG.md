# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Unreleased
- Create custom terminal executions, instead of using VSCode tasks.
- Only run pre-commit once, instead of separately for each check. Requires more flexible problem matchers or log files.

# Released
## [0.4.3] - 2022-05-11
### Fixed
- Use current workspace folder as the scope for tasks, not only as working directory.
## [0.4.2] - 2022-05-11
### Fixed
- Use current workspace folder as working directory for tasks.
## [0.4.1] - 2022-05-11
### Fixed
- Task reveal on error (disabled).
## [0.4.0] - 2022-05-11
### Removed
- Ability to select a custom shell for executing pre-commit. Please use the [VSCode option for configuring the task/debug shell](https://code.visualstudio.com/docs/editor/integrated-terminal#_configuring-the-taskdebug-profile) instead.
## [0.3.2] - 2022-05-06
### Added
- Ability to select a custom shell for executing pre-commit through this extension.
## [0.2.5] - 2022-04-22
### Fixed
- Folder-level settings now take precedence over workspace settings.
## [0.2.4] - 2022-04-14
### Fixed
- Extension settings were not available at folder-level when using a workspace (changed scope).
## [0.2.3] - 2022-03-22
### Added
- More console output for diagnostics.
## [0.2.2] - 2022-03-22
### Fixed
- Block repeated hook execution while setting up tasks. Task setup is not instant, so that there was a small time window for triggering repeated hook execution.
## [0.2.1] - 2022-03-22
### Fixed
- Regular expression matching on saving.
## [0.2.0] - 2022-03-22
### Added
- Ability to exclude hook IDs from execution.
### Changed
- Setting name for matching files on saving.
### Fixed
- Matching file names now only occurs when saving, not when running the extension from the command panel.
- Console log formatting.
## [0.1.8] - 2022-03-21
### Fixed
- Execution of task with skipped hooks (set environment variable correctly in PowerShell).
## [0.1.7] - 2022-03-21
### Fixed
- Windows path resolution bug (now using fsPath, instead of path on URIs).
- Diagnostic output of finished tasks
## [0.1.6] - 2022-03-21
### Fixed
- Resolve pre-commit configuration path on Windows.
## [0.1.5] - 2022-03-21
### Added
- Details to the documentation.
### Removed
- Keybindings. These would cause conflicts with existing bindings.
## [0.1.4] - 2022-03-21
### Fixed
- Running task detection: add unique task names with timestamps.
### Added
- Output for diagnosing the behaviour of the extension (`pre-commit` output tab).
## [0.1.3] - 2022-03-18
### Fixed
- Running of new tasks, while tasks are still running, is now prevented (found to be unstable, task names not unique).
## [0.1.2] - 2022-03-18
### Added
- Key bindings.
## [0.1.1] - 2022-03-18
### Added
- Many comments.
## [0.1.0] - 2022-03-17
### Added
- Initial codebase.
