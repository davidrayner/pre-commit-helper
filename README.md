# pre-commit

This extension executes `pre-commit` hooks, as defined by means of a configuration file for [https://pre-commit.com/](https://pre-commit.com/).
The `pre-commit-helper` parses `.pre-commit-config.yaml` files in a location of your choice.

# Types of hooks

This extension distinguishes between two different types of hooks: _fixes_ and _checks_.

## Differences

Fixes are expected to perform file operations, and change the content of files (e.g. formatters). Thus, fixes are executed
in sequence and are not allowed to run concurrently. Otherwise, multiple fixes may applied to the same file at the same time and lead to conflicts.

Checks are hooks that do not change the content of files, but merely check the files with a certain tool (e.g. linters). These hooks can
be executed concurrently. This has two benefits:
- Concurrent execution saves time
- Each of the hooks is an independent task with its own problem matcher

The latter allows to reliably show problems that are detected by the hooks in the _Problems_ panel.

## Detection of hook type

This extensions assumes the type of a hook, based on its `id`, `name` and `alias` strings, which can be summarized as the hook identification.

If a hook identification contains one of the substrings
- `fix`
- `format`
- `sort`

the hook is immediately considered a fix.

If a hook identification does not contain any of the previous substrings, but contains one of
- `mypy`
- `flake8`
- `spell`
- `check`
- `lint`
- `style`

the hook is considered a check.

If none of the above substrings are included in the hook identification, the hook is considered a fix as a fallback.

## Adjusting the hook type

In case that none of the mentioned substrings are included in the hook name, but you want to have it detected as a check, simply introduce an `alias`, which contains `check`.
For example, your hook might be called `my_special_hook`. Create an alias `my_special_hook_check` to make it a check.

If the hook is contained in the [official hook list](https://pre-commit.com/hooks) of pre-commit, but is not correctly identified by this extension, please open an [issue](https://gitlab.com/mic_public/tools/vscode-extensions/pre-commit-helper/-/issues).

# Interaction with the extension

This extension provides an automatic trigger of hooks on saving a file, as well as commands for manually triggering hook execution. See [the configuration section](#configuration), for setup.

Find the commands that are exposed, by typing `pre-commit` in the [command palette](https://code.visualstudio.com/docs/getstarted/userinterface#_command-palette). It gives you options to run pre-commit hooks (checks, fixes, or all hooks) on a selection of files (the current file, or all files).

# Configuration
## Select the `.pre-commit-config.yaml` file

You can set the `pre-commit-helper.config` variable to the path of your `.pre-commit-config.yaml` configuration, if it is not located in the workspace root (the default setting).
This path is relative to the currently open workspace folder, which is inferred from the file that is open in the editor.

Example:
```json
"pre-commit-helper.config" = "your/subfolder/.pre-commit-config.yaml"
```

## Use in Python virtual environments

When using Python virtual environments with VSCode, any terminal-based task is executed in that environment per default.

This is likely undesired for this extension, and can be disabled by means of
```json
"python.terminal.activateEnvironment": false
```

This is only a workaround and will be fixed in future versions of this extension.

## Use with a non-default shell

If you are using VSCode with a non-default shell (e.g. WSL, while running VSCode on Windows), then this extension will execute tasks in your selected default shell as well.

In that case, running pre-commit through this extension will likely fail, due to incorrect paths or lacking permissions. Use the corresponding VSCode setting for [changing the task and debug shell](https://code.visualstudio.com/docs/editor/integrated-terminal#_configuring-the-taskdebug-profile).


## Activity on saving a file

### Automatic trigger

For triggering hooks when saving a file, this extension allows to set the type of hooks to run on save by means of the configuration variable `pre-commit-helper.runOnSave`.
This can either be set to `fixes`, `checks`, `all hooks` or `none`.

Example:
```json
"pre-commit-helper.runOnSave" = "all hooks"
```
runs all hooks when saving a file.

### Filtering file types

For filtering the file types to check when saving, specify a regular expression in the configuration variable `pre-commit-helper.runOnSaveRegex`. The default matches all files. This parameter only filters files when _saving_, not when running an extension command from the command panel.

Example:
```json
"pre-commit-helper.runOnSaveRegex" = "^(.*)\\.(py|c|h)$"
```
only checks `*.py`, `*.c`, and `*.h` files when saving.

## Excluding hooks from execution

It is possible to exclude certain hook IDs from being executed by the extension. Set `pre-commit-helper.excludedIds` to an array of hooks that you don't want to run.

Example:
```json
"pre-commit-helper.excludedIds" = ["cppcheck", "pylint"]
```
excludes the `cppcheck` and `pylint` hook IDs from execution.
