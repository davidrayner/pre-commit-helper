/**
 * This module parses pre-commit config files and creates objects that each represent a hook.
 *
 * From these hooks, tasks are then created in the `tasks` module.
 */
import * as vscode from 'vscode';
import * as yaml from 'yaml';
import * as fs from 'fs';
import { join } from 'path';
import { knownCheckPatterns, knownFixPatterns } from './known_hook_patterns';
import { outputChannel } from './extension';

/**
 * A class that captures important properties of a pre-commit hook.
 */
export class PreCommitHook {
    id: string;
    name: string | undefined;
    alias: string | undefined;
    task: vscode.Task | undefined;
    excluded: boolean;

    /**
     * Creates a new PreCommitHook definition.
     *
     * @param id The id of the hook, as defined in the pre-commit config.
     * @param name The name of the hook, as defined in the pre-commit config.
     * @param alias The alias of the hook, as defined in the pre-commit config.
     * @param excluded If true, this hook is excluded from execution.
     * @param task A task that is attached to the hook.
     */
    constructor(
        id: string,
        name: string | undefined = undefined,
        alias: string | undefined = undefined,
        excluded: boolean = false,
        task: vscode.Task | undefined = undefined
    ) {
        this.id = id;
        this.name = name;
        this.alias = alias;
        this.excluded = excluded;
        this.task = task;
    }

    /**
     * A property that states, whether this hook is a check.
     *
     * @returns True, if this is a check, False otherwise.
     */
    get isCheck(): boolean {
        // Check for known patterns that define fixes first.
        // If any of the patterns matches, this is considered a fix.
        for (let knownFixPattern of knownFixPatterns) {
            if (this.id.includes(knownFixPattern)) {
                return false;
            }
        }

        // Then, check for known naming patterns of checks.
        for (let knownCheckPattern of knownCheckPatterns) {
            if (this.id.includes(knownCheckPattern)) {
                return true;
            }
        }

        // If no pattern matched, consider this a fix.
        return false;
    }

    /**
     * A property that states, whether this hook is a fix.
     *
     * @returns True, if this is a fix, False otherwise.
     */
    get isFix(): boolean {
        return !this.isCheck;
    }
};

/**
 * A class for parsing pre-commit configuration files and extracting hook definitions.
 */
export class PreCommitHookHandler {
    hooks: PreCommitHook[];
    excludedHookIds: string[] | undefined;

    constructor() {
        this.hooks = [];
        this.excludedHookIds = undefined;
    }

    get currentWorkspaceFolder(): vscode.WorkspaceFolder | undefined {
        let documentUri = vscode.window.activeTextEditor?.document.uri;

        if (documentUri) {
            return vscode.workspace.getWorkspaceFolder(documentUri);
        }
        else {
            return undefined;
        }
    }

    get currentWorkspaceFolderPath(): string | undefined {
        return this.currentWorkspaceFolder?.uri.fsPath;
    }

    /**
     * Extracts the configuration path from the settings.
     *
     * @returns The relative path to the pre-commit configurtion file.
     */
    get relativeConfigPath(): string {
        let relativeConfigPath: string | undefined =
            vscode.workspace.getConfiguration('pre-commit-helper', this.currentWorkspaceFolder).get("config");

        if (!relativeConfigPath) {
            // Default location of the pre-commit config, if not specified.
            relativeConfigPath = '.pre-commit-config.yaml';
        }

        return relativeConfigPath;
    }

    /**
     * Update the absolute path to the pre-commit configuration file.
     *
     * Takes into account the currently open file, and derives the active workspace from it.
     *
     * @returns the absolute file path, if valid, 'undefined' otherwise.
     */
    get absoluteConfigPath(): string | undefined {
        if (this.currentWorkspaceFolderPath) {
            return join(this.currentWorkspaceFolderPath, this.relativeConfigPath);
        } else {
            return undefined;
        }
    }

    /**
     * Parse the pre-commit configuration file, if there is a valid path to it.
     *
     * Detects hooks and creates hook definitions for them. These hook definitions are stored in the parser object.
     */
    public parseConfig() {
        // Reset known hooks first.
        this.hooks = [];

        if (!this.absoluteConfigPath) {
            return;
        }

        try {
            outputChannel.appendLine(`Use configuration file at '${this.absoluteConfigPath}'.`);
            var configFile: string = fs.readFileSync(this.absoluteConfigPath, 'utf8');
        } catch (error) {
            outputChannel.appendLine(`Did not find a configuration file at '${this.absoluteConfigPath}'.`);
            return;
        }

        let configContent = yaml.parse(configFile);

        this.excludedHookIds = vscode.workspace.getConfiguration('pre-commit-helper').get("excludedHookIds");

        configContent.repos.map((repo: any) => {
            repo.hooks.map(
                (hookDefinition: any) => {
                    this.addHook(hookDefinition);
                }
            );
        });
    }

    /**
     * Adds a hook to the internal list of hooks.
     *
     * @param hookDefinition The hook definition from which to create a PreCommitHook and add to the list of hooks.
     */
    private addHook(hookDefinition: any) {
        let excluded: boolean = false;
        if (this.excludedHookIds?.includes(hookDefinition.id)) {
            excluded = true;
        }

        this.hooks.push(
            new PreCommitHook(
                hookDefinition.id,
                hookDefinition.name,
                hookDefinition.alias,
                excluded
            )
        );
    }

    /**
     * Filter hooks by means of a decision function.
     *
     * Creates a new list of hooks that pass the filter.
     *
     * @param filter The filtering function to call on the hook.
     * @returns The hooks that passed the filter function.
     */
    private filterHooks(filter: CallableFunction): PreCommitHook[] {
        let filteredHooks: PreCommitHook[] = [];

        for (let hook of this.hooks) {
            if (filter(hook)) {
                filteredHooks.push(hook);
            }
        }

        return filteredHooks;
    }

    /**
     * Takes a provided list of hooks, and the list of all hooks, and calculates their difference.
     *
     * @param allHooks The list of all hooks.
     * @param excludedHooks The list of hooks to remove from the list of all hooks.
     */
    private remainingHooks(allHooks: PreCommitHook[], excludedHooks: PreCommitHook[]): PreCommitHook[] {
        let remainingHooks: PreCommitHook[] = [];

        for (let hook of allHooks) {
            if (!excludedHooks.includes(hook)) {
                remainingHooks.push(hook);
            }
        }

        return remainingHooks;
    }

    /**
     * Get the hooks from the list of hooks, which are checks and not excluded from execution.
     */
    get checkHooks(): PreCommitHook[] {
        return this.filterHooks((hook: PreCommitHook) => { return hook.isCheck && !hook.excluded; });
    }

    /**
     * Get the hooks, which are not within the list of checks. This also means excluded hooks.
     */
    get notCheckHooks(): PreCommitHook[] {
        return this.remainingHooks(this.hooks, this.checkHooks);
    }

    /**
     * Get the hooks from the list of hooks, which are fixes and not excluded from execution.
     */
    get fixHooks(): PreCommitHook[] {
        return this.filterHooks((hook: PreCommitHook) => { return hook.isFix && !hook.excluded; });
    }

    /**
     * Get the hooks, which are not within the list of fixes. This also means excluded hooks.
     */
    get notFixHooks(): PreCommitHook[] {
        return this.remainingHooks(this.hooks, this.fixHooks);
    }
}
